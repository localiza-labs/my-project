resource "google_compute_firewall" "firewall" {
  name    = "rule-firewall"
  network = google_compute_network.vpc.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "8080", "2376", "1000-2000"]
  }

  source_tags = ["web"]
  source_ranges = [ "0.0.0.0/0" ]
}
