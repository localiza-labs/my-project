# Deployment Application
resource "google_deployment_manager_deployment" "deployment" {
  name = "deployment-app"

  target {
    config {
      content = file("app/deploy.yaml")
    }
  }

  labels {
    key = "foo"
    value = "bar"
  }
}