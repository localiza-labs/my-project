#!/bin/sh

echo "Update"

sudo yum -y update
sudo yum install htop -y
sudo yum install wget -y

echo "Install Docker"

sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo -y
sudo yum install docker-ce docker-ce-cli containerd.io -y
sudo systemctl start docker

echo "Install Jenkins"

sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
sudo yum upgrade -y
sudo yum install jenkins java-1.8.0-openjdk-devel -y
sudo systemctl daemon-reload
sudo systemctl start jenkins

echo "Modify Daemon Docker"

sudo mkdir -p /etc/systemd/system/docker.service.d/

echo "install adicional"

sudo yum install kubectl -y
sudo yum install git -y
sudo usermod -aG docker ramon
sudo usermod -aG docker jenkins
sudo iptables -I INPUT -p tcp --dport 8080 -j ACCEPT
sudo iptables -I INPUT -p tcp --dport 2376 -j ACCEPT
























#local-exec { 
#  interpreter = ["/bin/bash" ,"-c"],
#  command = <<-EOT
#    exec "command1"
#    exec "command2"
#  EOT
#}