### PROJECT INFRA AS CODE

### REQUISITOS

1. Criar uma conta no Google Cloud
2. Efetuar as configurações de acesso a essa conta através de Client SDK em sua maquina
3. Efetuar a instalação do Terraform de acordo com a Versão listada nesse projeto em (my-project/terraform/versions.tf)
4. Ter instalado o Docker com DOcker Compose.


### CRIACAO DE IMAGENS DOCKER

Na execução do script com o comando (./script.sh), a execução desse arquivo fará todo o trabalho com interação do usuário na aprovação final de execução do terraform. 
Este script invoca comandos utilizando Docker para um build de imagens através de imagens base, adicionando assim variaveis e configurações.
Após build dessas imagens, as mesmas são enviadas para o registry Docker para ser utilizadas mais tarde.
Depois de subir as imagens ao registry, o script aciona o terraform que irá provisionar a infraestrutura.

### PROVISIONAMENTO DE INFRA NO GOOGLE CLOUD

Com a execução do Terraform, este começa a carregar os dados e solicita aprovação via terminal para dar continuidade ao processo de provisionamento. 
Após o aceite (yes), o terraform se incarrega de gerar toda a infra configurada, desde as redes envolvidas até o cluster kubernetes responsável por comportar as aplicações que nele será implantada.

### DEPLOY DA APLICACAO DENTRO DA CLOUD USANDO IMAGENS GERADAS

O deploy da aplicação também será executada juntamente com o provisionamento da Infra, este aplica o arquivo de Kubernetes fazendo assim o deploy das aplicações, com as imagens já criadas e disponibilizadas do registry Docker. 
Que após encerrar a execução do terraform estara disponivel para acesso de acordo com o IP originado dentro da Cloud atribuido a um Loadbalancer.

