#!/bin/bash
echo "Gerando Imagens"
cd wordpress_site1
docker-compose build
cd ..
cd wordpress_site2
docker-compose build
cd ..
cd wordpress_site3
docker-compose build
cd ..
cd mysql
docker-compose build
cd ..
echo "Realizando Push Imagens"
docker push ramon23sousa/wordpress:1.0
docker push ramon23sousa/wordpress:2.0
docker push ramon23sousa/wordpress:3.0
docker push ramon23sousa/mysql-5.7:latest
echo "Provisionando Ambiente Via Terraform"
cd terraform
terraform init
terraform apply
echo "Ambientes Provisionados!"